<?php

    require 'dbManager.php';

    //If a postID and a text is specified via POST, it will add a comment in the specified post and will redirect the user to the post.
    if(isset($_POST['postID']) && isset($_POST['text'])) {
        
        insertNewComment($_POST['postID'], $_POST['text']);

    }

    header("Location: feed.php?postID={$_POST['postID']}");

?>