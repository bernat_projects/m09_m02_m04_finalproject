<?php

    require 'dbManager.php';

    /* Checks if the user is the owner of the post. If it isn't the owner or the cookie doesn't exist,
    it will be redirected to the index page. If the user is the owner it will delete the post before redirecting it
    to the index page. */
    if(checkIfPostOwner($_GET['postID'], $_COOKIE['UserLoggedIn'])) {

        deletePost($_GET['postID']);

    }

    header("Location: index.php");

?>