<?php

    require 'dbManager.php';

    //Checks if a displayName parameter was introduced via POST.
    if(isset($_POST['displayName'])) {

        /* Checks if a image was introduced. If it was introduced, it will update the image and information of the profile.
        If not, it will only update the information. */
        if($_FILES['image']['name'] != null) {

            uploadImageToServer();
            updateProfileImage($_FILES['image']['name']);
            updateProfile($_POST['displayName'], $_POST['birthday'], $_POST['country'], $_POST['gender'], $_POST['description']);

        }
        else {

            updateProfile($_POST['displayName'], $_POST['birthday'], $_POST['country'], $_POST['gender'], $_POST['description']);

        }

    }

    //Uploads a image to the designed directory if the image complies with the requisites stated.
    function uploadImageToServer() {

        if (isset($_FILES['image']['name'])){ 
    
            if ((($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 50000000)) {
            
                $fileName = $_FILES['image']['name'];
                $uploadDirectory = "userImages/" . $fileName;
                $result = move_uploaded_file($_FILES["image"]["tmp_name"], $uploadDirectory);
                
            }
                
        }

    }

    header('Location: Personal-Information.php');

?>