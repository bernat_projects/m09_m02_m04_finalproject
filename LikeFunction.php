<?php

    require 'dbManager.php';

    /* Checks if the cookie UserLoggedIn exists. Then, checks if a postID or commentID was introduced via GET and performs the like function
    depending on the type of post and redirects the user to the URL they were before. */
    if(isset($_COOKIE['UserLoggedIn'])) {

        if(isset($_GET['postID'])) {

            likeFunctionality($_GET['postID']);
    
        }
        else if(isset($_GET['commentID'])) {

            likeFunctionalityComments($_GET['commentID']);
    
        }

        header("Location: {$_GET['url']}");

    }
    else {

        header('Location: index.php');

    }

?>