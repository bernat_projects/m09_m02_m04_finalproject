<?php

    require 'dbManager.php';

    //Checks if a text was introduced. If a text wasn't introduced, it will check if a image was introducted.
    if(isset($_POST['text'])) {

        //Checks if a image was introduced with the text.
        if(isset($_FILES['image'])) {

            //Uploads the image to the server and Updates the post with the text and image introduced.
            uploadImageToServer();
            updatePost($_POST['postID'], $_POST['text'], $_FILES['image']['name']);

        }
        else {

            //Updates the post with the text introduced.
            updatePost($_POST['postID'], $_POST['text'], null);

        }

    }
    else if(isset($_FILES['image'])) {

        //Uploads the image to the server and updates the post with the image introduced.
        uploadImageToServer();
        updatePost($_POST['postID'], null, $_FILES['image']['name']);

    }

    //Uploads a image to the designed directory if the image complies with the requisites stated.
    function uploadImageToServer() {

        if (isset($_FILES['image']['name'])){ 

            if ((($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 50000000)) {
            
                $fileName = $_FILES['image']['name'];
                $uploadDirectory = "userImages/" . $fileName;
                $result = move_uploaded_file($_FILES["image"]["tmp_name"], $uploadDirectory);
                
            }
                
        }

    }

    header("Location: feed.php?postID={$_POST['postID']}");

?>