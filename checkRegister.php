<?php

    require 'sessionManager.php';
    require 'dbManager.php';

    startSession("RegisterChecker");

    if(isset($_SESSION['email'])) {

        $emailResult = checkRegisterEmail($_SESSION['email']);
        $usernameResult = checkRegisterUsername($_SESSION['username']);
    
        if($emailResult != null) {
    
            //Returns the user to the register form and displays an error message.
            $_SESSION['emailUsed'] = true;
            header('Location: register.php');
    
        }
        else {
    
            if($usernameResult != null) {
    
                //Returns the user to the register form and displays an error message.
                $_SESSION['userExists'] = true;
                header('Location: register.php');
    
            }
            else {
    
                //Inserts the data introduced by the user in the collection and stores it in a variable.
                registerNewUser($_SESSION['username'], $_SESSION['email'], $_SESSION['password']);
                $usernameResult = checkCredentials($_SESSION['email'], $_SESSION['password']);
    
                //Creates the cookie "UserLoggedIn" which stores the _id from the document and destroys the session and the cookie "RegisterChecker".
                setcookie('UserLoggedIn', $usernameResult['_id'], time() + (3600 * 1), '/');
                setcookie('UsernameLoggedIn', $usernameResult['username'], time() + (3600 * 1), '/');
                session_destroy();
                setcookie('RegisterChecker', '', -1, '/');
    
                header('Location: index.php');
    
            }
    
        }

    }
    else {

        header('Location: login.php');

    }

    

?>