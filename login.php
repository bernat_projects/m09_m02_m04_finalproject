<?php

    require 'sessionManager.php';

    //Deletes the cookie RegisterChecker and starts the session CredentialsChecker.
    setcookie('RegisterChecker', '', -1, '/');
    startSession("CredentialsChecker");

    /* If a email and password were introduced via POST, it sets the SESSION email and password values as the POST values and
    redirects the user to the checkCredentials file. */
    if(array_key_exists("email", $_POST) && array_key_exists("password", $_POST)) {

        $_SESSION['email'] = $_POST['email'];
        $_SESSION['password'] = $_POST['password'];

        header('Location: checkCredentials.php');

    }

    //Checks if the cookie that represents that the user is logged exists. If it exists, it redirects the user to the index page.
    if(checkSessionCookie("UserLoggedIn")) {

        header('Location: index.php');

    }

    //Displays a error message if one of the credentials introduced were incorrect.
    function wrongCredentials() {

        if(array_key_exists("wrongCred", $_SESSION)) {

            echo "<p class=\"login100-form-incorrect\"> Incorrect email or password </p>";
            session_destroy();

        }

    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Login - Social Network</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">


    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/login/util.css">
    <link rel="stylesheet" type="text/css" href="css/login/main.css">

    <!-- Icons FontAwesome 4.7.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"  type="text/css" />




</head>
<body>
    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100">
                <div class="login100-pic js-tilt" data-tilt>
                    <a href="index.php"><img src="images/logo.png" alt=""></a>
                </div>

                <!-- Start of the login form -->
                <form class="login100-form validate-form" method="post">
                    <span class="login100-form-title">
                        Member Login
                    </span>

                    <?php wrongCredentials() ?>

                    <!-- Email field -->
                    <div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                        <input class="input100" type="email" name="email" placeholder="Email" required>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </span>
                    </div>

                    <!-- Password field -->
                    <div class="wrap-input100 validate-input" data-validate = "Password is required">
                        <input class="input100" type="password" name="password" placeholder="Password" required>
                        <span class="focus-input100"></span>
                        <span class="symbol-input100">
                            <i class="fa fa-lock" aria-hidden="true"></i>
                        </span>
                    </div>

                    <!-- Send from button -->
                    <div class="container-login100-form-btn">
                        <button class="login100-form-btn">
                            Login
                        </button>
                    </div>

                    <!-- Recover password link -->
                    <div class="text-center p-t-12">
                        <span class="txt1">
                            Forgot
                        </span>
                        <a class="txt2" href="#">
                            Username / Password?
                        </a>
                    </div>

                    <!-- Register link -->
                    <div class="text-center p-t-136">
                        <a class="txt2" href="register.php">
                            Create your Account
                            <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                        </a>
                    </div>
                    
                </form>
                <!-- End of the login form -->

            </div>
        </div>
    </div>



    <script src="js/jquery/jquery-3.2.1.min.js"></script>
    <script src="js/popper.js"></script>
    <script src="js/tilt.jquery.min.js"></script>
    <script >
        $('.js-tilt').tilt({
            scale: 1.1
        })
    </script>



</body>
</html>