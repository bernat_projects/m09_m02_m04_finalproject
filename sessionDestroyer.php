<?php

    //Destroys the cookie that stores the UserID and redirects the user to the login page.
    setcookie('UserLoggedIn', '', -1, '/');
    setcookie('UsernameLoggedIn', '', -1, '/');
    header('Location: login.php');

?>