<?php

    require 'dbManager.php';

    /* Checks if the user is the owner of the post. If it isn't the owner or the cookie doesn't exist,
    it will be redirected to the index page. If the user is the owner it will delete the image of the post
    before redirecting it to the modify post page. */
    if(checkIfPostOwner($_GET['postID'], $_COOKIE['UserLoggedIn'])) {

        deleteImage($_GET['postID']);
        header("Location: ModifyPost.php?postID={$_GET['postID']}");

    }
    else {

        header("Location: index.php");

    }

?>