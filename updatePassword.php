<?php

    require 'dbManager.php';

    /* Checks via post if a password was specified via POST. Then, checks if the password is correct and
    if the newPassword and confirmPassword match. If all the requisites meet, the password will change. */
    if(isset($_POST['currentPassword'])) {

        if(checkPassword($_POST['currentPassword'])) {

            if($_POST['newPassword'] == $_POST['confirmPassword']) {

                updatePassword($_POST['newPassword']);
                setcookie('PasswordUpdated', 'false', time() + (3600 * 1), '/');

            }
            else {

                setcookie('MatchingPassword', 'false', time() + (3600 * 1), '/');

            }

        }
        else {

            setcookie('WrongPassword', 'false', time() + (3600 * 1), '/');

        }

    }

    header('Location: Change-Password.php');

?>