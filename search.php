<?php

    require 'sessionManager.php';
    require 'dbManager.php';

    //Checks if the cookie that represents that the user is logged exists. If not, it redirects the user to the login page.
    if(!checkSessionCookie("UserLoggedIn")) {

        header('Location: login.php');

    }

    //Displays the "display name" of the user logged in.
    function displayName() {

        $result = findUserDataByID($_COOKIE['UserLoggedIn']);
        echo $result['displayName'];

    }

    //Displays the username of the user logged in.
    function displayUsername() {

        $result = findUserDataByID($_COOKIE['UserLoggedIn']);
        echo $result['username'];

    }

    //Displays the profile image of the user logged in.
    function displayImage() {

        $result = findUserDataByID($_COOKIE['UserLoggedIn']);
        echo $result['image'];

    }

    /* Gets the amount of follows and followers. If the amount surpasses 1000, it will perform a conversion factor to 
    avoid reaching a long number that will affect the appearence of the page. */
    function getFollowersAndFollows() {

        $result = getNumOfFollowersAndFollows($_COOKIE['UserLoggedIn']);
        $numFollowers = $result['numOfFollowers'];
        $numFollows = $result['numOfFollows'];
        $followersShort = "";
        $followsShort = "";
        $followersShortCount = 0;
        $followsShortCount = 0;

        while($numFollowers >= 1000 || $numFollows >= 1000) {

            if($numFollowers >= 1000) {

                $numFollowers /= 1000;
                $followersShortCount++;

                switch($followersShortCount) {

                    case 1:
                        $followersShort = "k";
                        break;
                    case 2:
                        $followersShort = "M";
                        break;
                    case 3:
                        $followersShort = "B";
                        break;

                }

            }

            if($numFollows >= 1000) {

                $numFollows /= 1000;
                $followsShortCount++;

                switch($followsShortCount) {

                    case 1:
                        $followsShort = "k";
                        break;
                    case 2:
                        $followsShort = "M";
                        break;
                    case 3:
                        $followsShort = "B";
                        break;

                }

            }

        }

        echo (floor($numFollowers * 10) / 10) . $followersShort . " followers / " . (floor($numFollows * 10) / 10) . $followsShort . " follows";

    }

    //Displays all the users that contain the keyword specified in their "display name" or username.
    function displayUsers($keyword) {

        $resultUsers = searchUsers($keyword);

        $allUsers = "";

        foreach($resultUsers as $user) {

            $userID = $user['_id'];

            $allUsers .= <<<EOT
            <div class="row border-radius">
            <div class="feed">
            <div class="feed_title">
            <img src="userImages/{$user['image']}"/>
            <span> <a href="profile.php?user={$user['username']}"> <b> {$user['displayName']} ({$user['username']}) </b> </a> </span>
            </div>
            </div>
            </div>
            EOT;

        }

        if($allUsers == "") {

            echo "<p style=\" display: flex; justify-content: center; font-weight: bold; \"> No users found </p>";

        }
        else {

            echo $allUsers;

        }


    }

    //Displays all the posts that contain the keyword specified in their text.
    function displayPosts($keyword) {

        $resultPosts = searchPosts($keyword);

        $allPosts = "";

        foreach($resultPosts as $post) {

            $postID = $post['_id'];
            $isLiked = isLiked("Post", $postID);
            $likeButton = "";
            $numOfLikes = getPostLikes($postID);
            $numOfComments = getComments($postID);

            if($isLiked) {

                $likeButton = <<<EOT
                <li> <a onmouseover="this.style.color='#515365';" onmouseout="this.style.color='#ff5e3a';" style=" color: #ff5e3a; " href="LikeFunction.php?postID={$postID}&url=index.php"> <i class="fa fa-heart"> </i> </a> {$numOfLikes} likes </li>
                EOT;

            }
            else {

                $likeButton = <<<EOT
                <li> <a onmouseover="this.style.color='#ff5e3a';" onmouseout="this.style.color='#515365';" style=" color: #515365; " href="LikeFunction.php?postID={$postID}&url=index.php"> <i class="fa fa-heart"> </i> </a> {$numOfLikes} likes </li>
                EOT;

            }

            if($post['authorData']['_id'] == $_COOKIE['UserLoggedIn']) {

                if($post['image'] == null) {
    
                    $allPosts .= <<<EOT
                    <div class="row border-radius">
                    <div class="feed">
                    <div class="feed_title">
                    <img src="userImages/{$post['authorData']['image']}"/>
                    <span> <a href="profile.php?user={$post['authorData']['username']}"> <b> {$post['authorData']['displayName']} ({$post['authorData']['username']}) </b> </a> <p> {$post['postDate']} </p> </span>
                    </div>
                    <a href="feed.php?postID={$postID}" style="color: #3F4257;">
                    <div class="feed_content">
                    <div class="feed_content_image">
                    <p> {$post['text']} </p>
                    </div>
                    </div>
                    </a>
                    <div class="feed_footer">
                    <ul class="feed_footer_left">
                    {$likeButton}
                    </ul>
                    <ul class="feed_footer_right">
                    <a href="ModifyPost.php?postID={$postID}" style="color:#515365;"><li class="hover-orange"> Edit post </li></a>
                    <a href="feed.php?postID={$postID}" style="color:#515365;"><li class="hover-orange"><i class="fa fa-comments-o"></i> {$numOfComments} comments</li></a>
                    </ul>
                    </div>
                    </div>
                    </div>
                    EOT;
        
                }
                else if($post['text'] == null) {
        
                    $allPosts .= <<<EOT
                    <div class="row border-radius">
                    <div class="feed">
                    <div class="feed_title">
                    <img src="userImages/{$post['authorData']['image']}"/>
                    <span> <a href="profile.php?user={$post['authorData']['username']}"> <b> {$post['authorData']['displayName']} ({$post['authorData']['username']}) </b> </a> <p> {$post['postDate']} </p> </span>
                    </div>
                    <a href="feed.php?postID={$postID}" style="color: #3F4257;">
                    <div class="feed_content">
                    <div class="feed_content_image">
                    <img src="userImages/{$post['image']}" alt="" />
                    </div>
                    </div>
                    </a>
                    <div class="feed_footer">
                    <ul class="feed_footer_left">
                    {$likeButton}
                    </ul>
                    <ul class="feed_footer_right">
                    <a href="ModifyPost.php?postID={$postID}" style="color:#515365;"><li class="hover-orange"> Edit post </li></a>
                    <a href="feed.php?postID={$postID}" style="color:#515365;"><li class="hover-orange"><i class="fa fa-comments-o"></i> {$numOfComments} comments</li></a>
                    </ul>
                    </div>
                    </div>
                    </div>
                    EOT;
        
                }
                else {
        
                    $allPosts .= <<<EOT
                    <div class="row border-radius">
                    <div class="feed">
                    <div class="feed_title">
                    <img src="userImages/{$post['authorData']['image']}"/>
                    <span> <a href="profile.php?user={$post['authorData']['username']}"> <b> {$post['authorData']['displayName']} ({$post['authorData']['username']}) </b> </a> <p> {$post['postDate']} </p> </span>
                    </div>
                    <a href="feed.php?postID={$postID}" style="color: #3F4257;">
                    <div class="feed_content">
                    <div class="feed_content_image">
                    <p> {$post['text']} </p>
                    </div>
                    <div class="feed_content_image">
                    <img src="userImages/{$post['image']}" alt="" />
                    </div>
                    </div>
                    </a>
                    <div class="feed_footer">
                    <ul class="feed_footer_left">
                    {$likeButton}
                    </ul>
                    <ul class="feed_footer_right">
                    <a href="ModifyPost.php?postID={$postID}" style="color:#515365;"><li class="hover-orange"> Edit post </li></a>
                    <a href="feed.php?postID={$postID}" style="color:#515365;"><li class="hover-orange"><i class="fa fa-comments-o"></i> {$numOfComments} comments</li></a>
                    </ul>
                    </div>
                    </div>
                    </div>
                    EOT;
        
                }
    
            }
            else {
    
                if($post['image'] == null) {
    
                    $allPosts .= <<<EOT
                    <div class="row border-radius">
                    <div class="feed">
                    <div class="feed_title">
                    <img src="userImages/{$post['authorData']['image']}"/>
                    <span> <a href="profile.php?user={$post['authorData']['username']}"> <b> {$post['authorData']['displayName']} ({$post['authorData']['username']}) </b> </a> <p> {$post['postDate']} </p> </span>
                    </div>
                    <a href="feed.php?postID={$postID}" style="color: #3F4257;">
                    <div class="feed_content">
                    <div class="feed_content_image">
                    <p> {$post['text']} </p>
                    </div>
                    </div>
                    </a>
                    <div class="feed_footer">
                    <ul class="feed_footer_left">
                    {$likeButton}
                    </ul>
                    <ul class="feed_footer_right">
                    <a href="feed.php?postID={$postID}" style="color:#515365;"><li class="hover-orange"><i class="fa fa-comments-o"></i> {$numOfComments} comments</li></a>
                    </ul>
                    </div>
                    </div>
                    </div>
                    EOT;
        
                }
                else if($post['text'] == null) {
        
                    $allPosts .= <<<EOT
                    <div class="row border-radius">
                    <div class="feed">
                    <div class="feed_title">
                    <img src="userImages/{$post['authorData']['image']}"/>
                    <span> <a href="profile.php?user={$post['authorData']['username']}"> <b> {$post['authorData']['displayName']} ({$post['authorData']['username']}) </b> </a> <p> {$post['postDate']} </p> </span>
                    </div>
                    <a href="feed.php?postID={$postID}" style="color: #3F4257;">
                    <div class="feed_content">
                    <div class="feed_content_image">
                    <img src="userImages/{$post['image']}" alt="" />
                    </div>
                    </div>
                    </a>
                    <div class="feed_footer">
                    <ul class="feed_footer_left">
                    {$likeButton}
                    </ul>
                    <ul class="feed_footer_right">
                    <a href="feed.php?postID={$postID}" style="color:#515365;"><li class="hover-orange"><i class="fa fa-comments-o"></i> {$numOfComments} comments</li></a>
                    </ul>
                    </div>
                    </div>
                    </div>
                    EOT;
        
                }
                else {
        
                    $allPosts .= <<<EOT
                    <div class="row border-radius">
                    <div class="feed">
                    <div class="feed_title">
                    <img src="userImages/{$post['authorData']['image']}"/>
                    <span> <a href="profile.php?user={$post['authorData']['username']}"> <b> {$post['authorData']['displayName']} ({$post['authorData']['username']}) </b> </a> <p> {$post['postDate']} </p> </span>
                    </div>
                    <a href="feed.php?postID={$postID}" style="color: #3F4257;">
                    <div class="feed_content">
                    <div class="feed_content_image">
                    <p> {$post['text']} </p>
                    </div>
                    <div class="feed_content_image">
                    <img src="userImages/{$post['image']}" alt="" />
                    </div>
                    </div>
                    </a>
                    <div class="feed_footer">
                    <ul class="feed_footer_left">
                    {$likeButton}
                    </ul>
                    <ul class="feed_footer_right">
                    <a href="feed.php?postID={$postID}" style="color:#515365;"><li class="hover-orange"><i class="fa fa-comments-o"></i> {$numOfComments} comments</li></a>
                    </ul>
                    </div>
                    </div>
                    </div>
                    EOT;
        
                }
    
            }

        }

        if($allPosts == "") {

            echo "<p style=\" display: flex; justify-content: center; font-weight: bold; margin-bottom: 25px; \"> No posts found </p>";

        }
        else {

            echo $allPosts;

        }

        

    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title>Newsfeed - Social Network</title>
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">


    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- Icons FontAwesome 4.7.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"  type="text/css" />



</head>
<body>
    <div class="navbar">
        <div class="navbar_menuicon" id="navicon">
            <i class="fa fa-navicon"></i>
        </div>
        <div class="navbar_logo">
            <img src="images/logo.png" alt="" />
        </div>
        <div class="navbar_page">
            <span>> SEARCH</span>
        </div>
        <div class="navbar_search">
            <form method="post" action="search.php">
                <input type="text" name="keyword" placeholder="Search other users and posts" />
                <button><i class="fa fa-search"></i></button>
            </form>
        </div>
        <div class="navbar_icons">
        </div>
        <div class="navbar_user" id="profilemodal" style="cursor:pointer">
            <img src="userImages/<?php displayImage() ?>" alt="" />
            <span id="navbar_user_top"> <?php displayName() ?> <br><p> <?php displayUsername() ?> </p></span><i class="fa fa-angle-down"></i>
        </div>
    </div>

    <div class="all">

        <div class="rowfixed"></div>
        <div class="left_row">
            <div class="left_row_profile">
                <img id="portada" src="images/portada.jpg" />
                <div class="left_row_profile">
                    <img id="profile_pic" src="userImages/<?php displayImage() ?>" />
                    <span> <?php displayName() ?> <br><p> <?php getFollowersAndFollows() ?> </p></span>
                </div>
            </div>
            <div class="rowmenu">
                <ul>
                    <li><a href="index.php"><i class="fa fa-globe"></i>Newsfeed</a></li>
                    <li><a href="profile.php"><i class="fa fa-user"></i>Profile</a></li>
                </ul>
            </div>
        </div>



        <div class="right_row">
            <div class="row">
                <div class="row_title" style=" margin-bottom: 20px; ">
                    <span><i class="fa fa-newspaper-o" aria-hidden="true"></i> Users </span>
                </div>
                
                <?= displayUsers($_POST['keyword']); ?>

                <div class="row_title" style=" margin-bottom: 20px; ">
                    <span><i class="fa fa-newspaper-o" aria-hidden="true"></i> Posts </span>
                </div>

                <?= displayPosts($_POST['keyword']); ?>

            </div>

        </div>

        <div class="suggestions_row">

        </div>

    </div>

    <button onclick="topFunction()" id="myBtn" title="Go to top"><i class="fa fa-arrow-up"></i></button>

    <!-- Modal Messages -->
    <div class="modal modal-comments">
        <div class="modal-icon-select"><i class="fa fa-sort-asc" aria-hidden="true"></i></div>
        <div class="modal-title">
            <span>CHAT / MESSAGES</span>
             <a href="messages.html"><i class="fa fa-ellipsis-h"></i></a>
        </div>
        <div class="modal-content">
            <ul>
                <li>
                    <a href="#">
                        <img src="images/user-7.jpg" alt="" />
                        <span><b>Diana Jameson</b><br>Hi James! It’s Diana, I just wanted to let you know that we have to reschedule...<p>4 hours ago</p></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="images/user-6.jpg" alt="" />
                        <span><b>Elaine Dreyfuss</b><br>We’ll have to check that at the office and see if the client is on board with...<p>Yesterday at 9:56pm</p></span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="images/user-3.jpg" alt="" />
                        <span><b>Jake Parker</b><br>Great, I’ll see you tomorrow!.<p>4 hours ago</p></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Modal Friends -->
    <div class="modal modal-friends">
        <div class="modal-icon-select"><i class="fa fa-sort-asc" aria-hidden="true"></i></div>
        <div class="modal-title">
            <span>FRIEND REQUESTS</span>
             <a href="friends.html"><i class="fa fa-ellipsis-h"></i></a>
        </div>
        <div class="modal-content">
            <ul>
                <li>
                    <a href="#">
                        <img src="images/user-2.jpg" alt="" />
                        <span><b>Tony Stevens</b><br>4 Friends in Common</span>
                        <button class="modal-content-accept">Accept</button><button class="modal-content-decline">Decline</button>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="images/user-6.jpg" alt="" />
                        <span><b>Tamara Romanoff</b><br>2 Friends in Common</span>
                        <button class="modal-content-accept">Accept</button><button class="modal-content-decline">Decline</button>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="images/user-4.jpg" alt="" />
                        <span><b>Nicholas Grissom</b><br>1 Friend in Common</span>
                        <button class="modal-content-accept">Accept</button><button class="modal-content-decline">Decline</button>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- Modal Profile -->
    <div class="modal modal-profile">
        <div class="modal-icon-select"><i class="fa fa-sort-asc" aria-hidden="true"></i></div>
        <div class="modal-title">
            <span>YOUR ACCOUNT</span>
             <a href="Personal-Information.php"><i class="fa fa-cogs"></i></a>
        </div>
        <div class="modal-content">
            <ul>
                <li>
                    <a href="Personal-Information.php">
                        <i class="fa fa-tasks" aria-hidden="true"></i>
                        <span><b>Profile Settings</b><br>Yours profile settings</span>
                    </a>
                </li>
                <li>
                    <a href="sessionDestroyer.php">
                        <i class="fa fa-power-off" aria-hidden="true"></i>
                        <span><b>Log Out</b><br>Close your session</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    
    <!-- NavMobile -->
    <div class="mobilemenu">
        
        <div class="mobilemenu_profile">
            <img id="mobilemenu_portada" src="images/portada.jpg" />
            <div class="mobilemenu_profile">
                <img id="mobilemenu_profile_pic" src="images/user.jpg" /><br>
                <span> <?php displayName() ?> <br><p>150k followers / 50 follow</p></span>
            </div>
            <div class="mobilemenu_menu">
                <ul>
                    <li><a href="index.php" id="mobilemenu-selected"><i class="fa fa-globe"></i>Newsfeed</a></li>
                    <li><a href="profile.php"><i class="fa fa-user"></i>Profile</a></li>
                    <li><a href="friends.html"><i class="fa fa-users"></i>Friends</a></li>
                    <li><a href="messages.html"><i class="fa fa-comments-o"></i>messages</a></li>
                    <li class="primarymenu"><i class="fa fa-compass"></i>Explore</li>
                    <ul class="mobilemenu_child">
                        <li style="border:none"><a href="#"><i class="fa fa-globe"></i>Activity</a></li>
                        <li style="border:none"><a href="#"><i class="fa fa-file"></i>Friends</a></li>
                        <li style="border:none"><a href="#"><i class="fa fa-file"></i>Groups</a></li>
                        <li style="border:none"><a href="#"><i class="fa fa-file"></i>Pages</a></li>
                        <li style="border:none"><a href="#"><i class="fa fa-file"></i>Saves</a></li>
                    </ul>
                    <li class="primarymenu"><i class="fa fa-user"></i>Rapid Access</li>
                    <ul class="mobilemenu_child">
                        <li style="border:none"><a href="#"><i class="fa fa-star-o"></i>Your-Page.html</a></li>
                        <li style="border:none"><a href="#"><i class="fa fa-star-o"></i>Your-Group.html</a></li>
                    </ul>
                </ul>
                    <hr>
                <ul>
                    <li><a href="#">Terms & Conditions</a></li>
                    <li><a href="#">FAQ's</a></li>
                    <li><a href="#">Contact</a></li>
                    <li><a href="sessionDestroyer.php">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
    // Modals
    $(document).ready(function(){


        $("#messagesmodal").hover(function(){
            $(".modal-comments").toggle();
        });
        $(".modal-comments").hover(function(){
            $(".modal-comments").toggle();
        });



        $("#friendsmodal").hover(function(){
            $(".modal-friends").toggle();
        });
        $(".modal-friends").hover(function(){
            $(".modal-friends").toggle();
        });


        $("#profilemodal").hover(function(){
            $(".modal-profile").toggle();
        });
        $(".modal-profile").hover(function(){
            $(".modal-profile").toggle();
        });


        $("#navicon").click(function(){
            $(".mobilemenu").fadeIn();
        });
        $(".all").click(function(){
            $(".mobilemenu").fadeOut();
        });
    });
    </script>
    <script>
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
                document.getElementById("myBtn").style.display = "block";
            } else {
                document.getElementById("myBtn").style.display = "none";
            }
        }

        function topFunction() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }
    </script>
</body>
</html>