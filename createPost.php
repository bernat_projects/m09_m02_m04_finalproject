<?php

    require 'dbManager.php';

    //Checks if a text was introduced. If a text wasn't introduced, it will check if a image was introducted.
    if(isset($_POST['text'])) {

        //Checks if a image was introduced with the text
        if(isset($_FILES['image'])) {

            //Uploads the image to the server and creates the document in the database with the text and the image introduced
            uploadImageToServer();
            insertNewPost($_POST['text'], $_FILES['image']['name']);
            header("Location: {$_POST['url']}");
            exit;

        }
        else {

            //Creates the document in the database with the text introduced
            insertNewPost($_POST['text'], null);
            header("Location: {$_POST['url']}");
            exit;

        }

    }
    else if(isset($_FILES['image'])) {

        //Uploads the image to the server and creates the document in the database with the image introduced
        uploadImageToServer();
        insertNewPost(null, $_FILES['image']['name']);
        header("Location: {$_POST['url']}");
        exit;

    }

    //Uploads a image to the designed directory if the image complies with the requisites stated.
    function uploadImageToServer() {

        if (isset($_FILES['image']['name'])){ 
    
            if ((($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 50000000)) {
            
                $fileName = $_FILES['image']['name'];
                $uploadDirectory = "userImages/" . $fileName;
                $result = move_uploaded_file($_FILES["image"]["tmp_name"], $uploadDirectory);
                
            }
                
        }

    }

    header('Location: index.php');

?>