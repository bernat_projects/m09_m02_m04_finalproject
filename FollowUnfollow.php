<?php

    require 'dbManager.php';

    /* Checks if a follow parameter was specified via POST. If it was specified, it will perform the function followFunctionality 
    to follow or unfollow a user. If not specified, it redirects the user to the index. */
    if(isset($_POST['follow'])) {

        followFunctionality($_POST['follow']);
        header("Location: {$_POST['url']}");

    }
    else {

        header('Location: index.php');

    }

?>