<?php

    //Set the name of a new session and starts it.
    function startSession($sessionName) {

        if(!$sessionName == null) session_name($sessionName);
        
        session_start();

    }

    //Checks if a session exists by checking the cookies.
    function checkSessionCookie($cookieName) {

        if(array_key_exists($cookieName, $_COOKIE)) {

            return true;

        }
        else {

            return false;

        }

    }

?>