<?php

    require 'MongoDB/vendor/autoload.php';

    //Makes a query that finds the document that matches the email and password specified.
    function checkCredentials($email, $password) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;
        $result = $collectionUsers -> findOne( [ 'email' => $email, 'password' => $password ] );

        return $result;

    }

    //Makes a query that finds the document that matches the email.
    function checkRegisterEmail($email) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;
        $result = $collectionUsers -> findOne( [ 'email' => $email ] );

        return $result;

    }

    //Makes a query that finds the document that matches the username.
    function checkRegisterUsername($username) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;
        $result = $collectionUsers -> findOne( [ 'username' => '@' . $username ] );
        echo $username;

        return $result;

    }

    //Inserts a new document at the users collection with the username, email and password introduced by the user.
    function registerNewUser($username, $email, $password) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;
        $collectionUsers -> insertOne( [ 'displayName' => $username, 'username' => "@" . $username, 'email' => $email, 'password' => $password, 'followers' => [], 'follows' => [], 'image' => 'defaultUser.png', 'birthday' => '', 'country' => '', 'gender' => '', 'description' => '' ] );

    }

    /* Makes a query that finds the document that matches the _id. If none is found, it will redirect the
    user to the login form after destroying the cookies. */
    function findUserDataByID($userID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;
        $result = $collectionUsers -> findOne( [ '_id' => new MongoDB\BSON\ObjectID($userID) ] );

        if($result == null) {

            header('Location: sessionDestroyer.php');

        }
        else {

            return $result;

        }

    }

    /* Makes a query that finds the document that matches the username. If none is found, it will redirect
    the user to the index page. */
    function findUserDataByUsername($username) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;
        $result = $collectionUsers -> findOne( [ 'username' => $username ] );

        if($result == null) {

            header('Location: index.php');

        }
        else {

            return $result;

        }

    }

    //Makes a query that gets the number of followers and follows of the user.
    function getNumOfFollowersAndFollows($userID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;

        $query = $collectionUsers -> aggregate( [ [ '$match' => [ '_id' => new MongoDB\BSON\ObjectId($userID) ] ], [ '$project' => [ 'numOfFollowers' => [ '$size' => '$followers' ], 'numOfFollows' => [ '$size' => '$follows' ] ] ] ] );

        $result;

        foreach($query as $document) {

            $result = $document;

        }

        return $result;

    }

    /* Makes a query that finds all the _id of the follows of the user. Then, it adds them in an array with the user's _id
    and makes a query that finds all the posts and sorts them by date, showing first the newest posts. */
    function findPosts() {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;

        $relevantUserID = $collectionUsers -> aggregate( [ ['$match' => [ '_id' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ] ], [ '$unwind' => '$follows' ] ] );

        $arrayUserID[] = new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']);
        foreach($relevantUserID as $userID) {

            array_push($arrayUserID, $userID['follows']);

        }

        $collectionPosts = $db -> posts;

        $result = $collectionPosts -> aggregate( [ [ '$match' => [ 'authorID' => [ '$in' => $arrayUserID ] ] ], [ '$lookup' => [ 'from' => 'users', 'localField' => 'authorID', 'foreignField' => '_id', 'as' => 'authorData' ] ], [ '$unwind' => '$authorData' ], [ '$set' => [ 'postDate' => [ '$dateToString' => [ 'date' => '$date', 'format' => '%B %d of %Y at %H:%M', 'timezone' => 'Europe/Madrid' ] ] ] ], [ '$sort' => [ 'date' => -1 ] ] ] );

        return $result;

    }

    /* Makes a query that finds the _id of a user by using his username. Then, it makes a query by using the _id
    to find all the posts of the user. */
    function findOneUserPosts($username) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;
        $userID = $collectionUsers -> findOne( [ 'username' => $username ] );

        $collectionPosts = $db -> posts;
        $result = $collectionPosts -> aggregate( [ [ '$match' => [ 'authorID' => new MongoDB\BSON\ObjectId($userID['_id']) ] ], [ '$lookup' => [ 'from' => 'users', 'localField' => 'authorID', 'foreignField' => '_id', 'as' => 'authorData' ] ], [ '$unwind' => '$authorData' ], [ '$set' => [ 'postDate' => [ '$dateToString' => [ 'date' => '$date', 'format' => '%B %d of %Y at %H:%M', 'timezone' => 'Europe/Madrid' ] ] ] ], [ '$sort' => [ 'date' => -1 ] ] ] );

        return $result;

    }

    /* Makes a query that finds a post by using the _id of the post. If the _id introduced hasn't has the correct format,
    it will redirect the user to the index page. */
    function findOnePost($postID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;

        $collectionPosts = $db -> posts;
        try {

            $result = $collectionPosts -> aggregate( [ [ '$match' => [ '_id' => new MongoDB\BSON\ObjectId($postID) ] ], [ '$lookup' => [ 'from' => 'users', 'localField' => 'authorID', 'foreignField' => '_id', 'as' => 'authorData' ] ], [ '$unwind' => '$authorData' ], [ '$set' => [ 'postDate' => [ '$dateToString' => [ 'date' => '$date', 'format' => '%B %d of %Y at %H:%M', 'timezone' => 'Europe/Madrid' ] ] ] ] ] );

            return $result;

        } catch(Exception) {

            header('Location: index.php');

        }

    }

    //Makes a query that finds the comments of a post by using the _id of the post.
    function findComments($postID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;

        $collectionPosts = $db -> posts;
        $result = $collectionPosts -> aggregate( [ [ '$match' => [ '_id' => new MongoDB\BSON\ObjectId($postID) ] ], [ '$project' => [ 'comments' => 1, '_id' => 0 ] ], [ '$unwind' => '$comments' ], [ '$lookup' => [ 'from' => 'users', 'localField' => 'comments.authorID', 'foreignField' => '_id', 'as' => 'authorData' ] ], [ '$unwind' => '$authorData' ], [ '$set' => [ 'commentDate' => [ '$dateToString' => [ 'date' => '$comments.date', 'format' => '%B %d of %Y at %H:%M', 'timezone' => 'Europe/Madrid' ] ] ] ], [ '$sort' => [ 'comments.date' => -1 ] ] ] );

        return $result;

    }

    //Inserts a new document at the posts collection with the text and image introduced by the user.
    function insertNewPost($text, $image) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;

        $authorID = $_COOKIE['UserLoggedIn'];

        $collectionPosts -> insertOne( [ 'text' => $text, 'image' => $image, 'authorID' => new MongoDB\BSON\ObjectId($authorID), 'date' => new MongoDB\BSON\UTCDateTime(), 'likes' => [], 'comments' => [] ] );

    }

    //Inserts a new object inside a the post's document with the text introduced by the user.
    function insertNewComment($postID, $text) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;

        $authorID = $_COOKIE['UserLoggedIn'];

        $collectionPosts -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($postID) ], [ '$push' => [ 'comments' => [ '_id' => new MongoDB\BSON\ObjectId(), 'text' => $text, 'authorID' => new MongoDB\BSON\ObjectId($authorID), 'date' => new MongoDB\BSON\UTCDateTime(), 'likes' => [] ] ] ] );

    }

    //Makes a query that finds the number of likes that a post has.
    function getPostLikes($postID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;

        $result = $collectionPosts -> aggregate( [ [ '$match' => [ '_id' => new MongoDB\BSON\ObjectId($postID) ] ], [ '$project' => [ 'numOfLikes' => [ '$size' => '$likes' ] ] ] ] );
        
        $numOfLikes;

        foreach($result as $post) {

            $numOfLikes = $post -> numOfLikes;

        }

        return $numOfLikes;

    }

    //Makes a query that finds the number of likes that a comment has.
    function getCommentLikes($commentID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;

        $result = $collectionPosts -> aggregate( [ [ '$match' => [ 'comments._id' => new MongoDB\BSON\ObjectId($commentID) ] ], [ '$unwind' => '$comments' ], [ '$project' => [ 'numOfLikes' => [ '$size' => '$comments.likes' ] ] ] ] );
        
        $numOfLikes;

        foreach($result as $comment) {

            $numOfLikes = $comment -> numOfLikes;

        }

        return $numOfLikes;

    }

    //Makes a query that gets all the comments of a post.
    function getComments($postID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;

        $result = $collectionPosts -> aggregate( [ [ '$match' => [ '_id' => new MongoDB\BSON\ObjectId($postID) ] ], [ '$project' => [ 'numOfComments' => [ '$size' => '$comments' ] ] ] ] );
        
        $numOfComments;

        foreach($result as $post) {

            $numOfComments = $post -> numOfComments;

        }

        return $numOfComments;

    }

    //Makes a query that checks if the current user has liked the post or comment.
    function isLiked($type, $objectID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;

        $check;

        if($type == "Post") {

            $check = $collectionPosts -> findOne( [ '_id' => new MongoDB\BSON\ObjectId($objectID), 'likes' => [ '$in' => [new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn'])] ] ] );

        }
        else if ($type == "Comment") {

            $check = $collectionPosts -> findOne( [ 'comments._id' => new MongoDB\BSON\ObjectId($objectID), 'comments.likes' => [ '$in' => [new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn'])] ] ] );

        }

        if($check == null) {

            return false;

        }
        else {

            return true;

        }

    }

    /* Makes a query that finds out if a user has liked the post. If it didn't liked the post,
    it will push its _id in the likes array. If it liked the post, it will pull the _id. */
    function likeFunctionality($postID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;

        $check = isLiked("Post", $postID);

        if(!$check) {

            $collectionPosts -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($postID) ], [ '$push' => [ 'likes' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ] ] );

        }
        else {

            $collectionPosts -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($postID) ], [ '$pull' => [ 'likes' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ] ] );

        }

    }

    /* Makes a query that finds out if a user has liked the comment. If it didn't liked the post,
    it will push its _id in the likes array. If it liked the post, it will pull the _id. */
    function likeFunctionalityComments($commentID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;

        $check = isLiked("Comment", $commentID);

        if(!$check) {

            $collectionPosts -> updateOne( [ 'comments._id' => new MongoDB\BSON\ObjectId($commentID) ], [ '$push' => [ 'comments.$.likes' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ] ] );

        }
        else {

            $collectionPosts -> updateOne( [ 'comments._id' => new MongoDB\BSON\ObjectId($commentID) ], [ '$pull' => [ 'comments.$.likes' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ] ] );

        }

    }

    //Makes a query that gets if the current user is following certain user.
    function isFollowing($username) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;

        $checkUserID = findUserDataByUsername($username);

        $checkFollow = $collectionUsers -> findOne( [ '_id' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']), 'follows' => [ '$in' => [new MongoDB\BSON\ObjectId($checkUserID['_id'])] ] ] );

        if($checkFollow == null) {

            return false;

        }
        else {

            return true;

        }

    }

    /* If the current user isn't following the other user, it will push the _id of the current user on 
    the followers array of the other user and will push the _id of the other user on the follows array of the current user.
    If the current user is following the other user, it will perform a pull instead. */
    function followFunctionality($username) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;

        $userID = findUserDataByUsername($username);
        $checkFollow = isFollowing($username);

        if(!$checkFollow) {

            $collectionUsers -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ], [ '$push' => [ 'follows' => new MongoDB\BSON\ObjectId($userID['_id']) ] ] );
            $collectionUsers -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($userID['_id']) ], [ '$push' => [ 'followers' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ] ] );

        }
        else
        {

            $collectionUsers -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ], [ '$pull' => [ 'follows' => new MongoDB\BSON\ObjectId($userID['_id']) ] ] );
            $collectionUsers -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($userID['_id']) ], [ '$pull' => [ 'followers' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ] ] );

        }

    }

    //Updates the document of the current user with the data introduced at the form.
    function updateProfile($displayName, $birthday, $country, $gender, $description) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;

        $collectionUsers -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ], [ '$set' => [ 'displayName' => $displayName, 'birthday' => $birthday, 'country' => $country, 'gender' => $gender, 'description' => $description ] ] );

    }

    //Updates the current image field of the user's document.
    function updateProfileImage($image) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;

        $collectionUsers -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ], [ '$set' => [ 'image' => $image ] ] );

    }

    //Sets the current image field of the user's document as the default image value.
    function defaultImage() {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;

        $collectionUsers -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ], [ '$set' => [ 'image' => 'defaultUser.png' ] ] );

    }

    //Makes a query that checks if the password introduced by the user is correct.
    function checkPassword($password) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;
        $result = $collectionUsers -> findOne( [ '_id' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']), 'password' => $password ] );

        if($result == null) {

            return false;

        }
        else {

            return true;

        }

    }

    //Updates the password field of the user's document.
    function updatePassword($password) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;
        $collectionUsers -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($_COOKIE['UserLoggedIn']) ], [ '$set' => [ 'password' => $password ] ] );

    }

    //Updates the text and image fields of the post's document if they were setted at the form.
    function updatePost($postID, $text, $image) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;

        if($text != null) {

            $collectionPosts -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($postID) ], [ '$set' => [ 'text' => $text ] ] );

        }

        if($image != null) {

            $collectionPosts -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($postID) ], [ '$set' => [ 'image' => $image ] ] );

        }

    }

    //Sets the image field of the post's document as a empty string.
    function deleteImage($postID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;
        $collectionPosts -> updateOne( [ '_id' => new MongoDB\BSON\ObjectId($postID) ], [ '$set' => [ 'image' => '' ] ] );

    }

    //Deletes the document of the current post, deleting the current post.
    function deletePost($postID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;
        $collectionPosts -> deleteOne( [ '_id' => new MongoDB\BSON\ObjectId($postID) ] );

    }

    //Makes a query that finds the users that match the keyword at the displayName and username fields.
    function searchUsers($keyword) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionUsers = $db -> users;
        $regex = new MongoDB\BSON\Regex($keyword, 'i');
        $result = $collectionUsers -> find( [ '$or' => [ [ 'displayName' => $regex ], [ 'username' => $regex ] ] ] );

        return $result;

    }

    //Makes a query that finds the post that match the keyword at the text field.
    function searchPosts($keyword) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;
        $regex = new MongoDB\BSON\Regex($keyword, 'i');
        $result = $collectionPosts -> aggregate( [ [ '$match' => [ 'text' => $regex ] ], [ '$lookup' => [ 'from' => 'users', 'localField' => 'authorID', 'foreignField' => '_id', 'as' => 'authorData' ] ], [ '$unwind' => '$authorData' ], [ '$set' => [ 'postDate' => [ '$dateToString' => [ 'date' => '$date', 'format' => '%B %d of %Y at %H:%M', 'timezone' => 'Europe/Madrid' ] ] ] ], [ '$sort' => [ 'date' => -1 ] ] ] );

        return $result;

    }

    //Makes a query that checks if the current user is the author of the post.
    function checkIfPostOwner($postID, $userID) {

        $client = new MongoDB\Client("mongodb://localhost:27017");
        $db = $client -> SocialNetwork;
        $collectionPosts = $db -> posts;
        $result = $collectionPosts -> findOne( [ '$and' => [ [ '_id' => new MongoDB\BSON\ObjectId($postID) ], [ 'authorID' => new MongoDB\BSON\ObjectId($userID) ] ] ] );

        if($result == null) {

            return false;

        }
        else {

            return true;

        }

    }

?>