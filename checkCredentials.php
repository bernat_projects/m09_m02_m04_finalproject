<?php

    require 'sessionManager.php';
    require 'MongoDB/vendor/autoload.php';

    //Starts the CredentialsChecker session.
    startSession("CredentialsChecker");

    //Creates a connection to the MongoDB desired database and collection.
    $client = new MongoDB\Client("mongodb://localhost:27017");
    $db = $client -> SocialNetwork;
    $collection = $db -> users;

    //Makes a query that finds the document that matches the email and password introduced by the user.
    $result = $collection -> findOne( [ 'email' => $_SESSION['email'], 'password' => $_SESSION['password'] ] );

    //If the query can't find a user, it will redirect the user to the login page and will display a error message.
    if($result == null) {

        $_SESSION['wrongCred'] = true;
        header('Location: login.php');

    }
    //Destroys the cookie of the session and creates two more with the _id and username of the user. Then, redirects the user to the index.
    else {

        session_destroy();
        setcookie('CredentialsChecker', '', -1, '/');

        setcookie('UserLoggedIn', $result['_id'], time() + (3600 * 1), '/');
        setcookie('UsernameLoggedIn', $result['username'], time() + (3600 * 1), '/');

        header('Location: index.php');

    }

?>